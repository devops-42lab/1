#!/bin/bash

log_message() {
    local message="$1"
    echo "$message" | tee -a script.log
}

while getopts ":d:" opt; do
    case ${opt} in
        d )
            root_dir=$OPTARG
            ;;
        \? )
            log_message "Невалидный параметр: -$OPTARG"
            exit 1
            ;;
        : )
            log_message "Параметр -$OPTARG нуждается в аргументе."
            exit 1
            ;;
    esac
done

if [ -z "$root_dir" ]; then
    read -p "Введите корневую директорию: " root_dir
fi

if [ ! -d "$root_dir" ]; then
    log_message "Корневая директория $root_dir не существует."
    exit 1
fi

users=$(cut -d: -f1 /etc/passwd)

for user in $users; do
    user_dir="$root_dir/$user"
    if [ ! -d "$user_dir" ]; then
        mkdir "$user_dir"
        if [ $? -eq 0 ]; then
            chown "$user:$user" "$user_dir"
            chmod 755 "$user_dir"
            log_message "Директория $user_dir создана и настроена для пользователя $user."
        else
            log_message "Ошибка при создании директории $user_dir."
        fi
    else
        log_message "Директория $user_dir уже существует."
    fi
done

